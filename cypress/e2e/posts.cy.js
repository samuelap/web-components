/// <reference types="Cypress" />

describe("posts e2e", () => {
  it("add post", () => {
    cy.visit("http://localhost:8080");
    cy.get("#navPost").click();
    cy.get("#formTitle").type("Nuevo");
    cy.get("#formBody").type("Hola Mundo");
    cy.get("#btnAdd").click();
    cy.get("#post-101 > post-ui > #title").should("exist");
  });

  it("delete post", () => {
    cy.visit("http://localhost:8080");
    cy.get("#navPost").click();
    cy.get("#post-1 > post-ui > #title").click();
    cy.get("#btnDelete").click();
    cy.get("#post-1 > post-ui > #title").should("not.exist");
  });

  it("update post", () => {
    cy.visit("http://localhost:8080");
    cy.get("#navPost").click();
    cy.get("#post-1 > post-ui > #title").click();
    cy.get("#formTitle").clear().type("Actualizado");
    cy.get("#formBody").clear().type("Hola Mundo v.2");
    cy.get("#btnUpdate").click();
    cy.get("#post-1 > post-ui > #title").should("exist");
    cy.get("#post-1 > post-ui > #title").should("contain.text", "Actualizado");
    cy.get("#btnCancel").click();
    cy.get("#formTitle").should("not.include.text");
    cy.get("#formBody").should("not.include.text");
    cy.get("#post-1 > post-ui > #title").click();
    cy.get("#formTitle").should("have.value", "Actualizado");
    cy.get("#formBody").should("have.value", "Hola Mundo v.2");
  });
});
