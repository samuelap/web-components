import { html, LitElement } from "lit";
import { v4 } from "uuid";

import { AddPostUseCase } from "../usecases/add-post.usecase";
import { DeletePostUseCase } from "../usecases/delete-post.usecase";
import { UpdatePostUseCase } from "../usecases/update-post.usecase";

export class PostActionsComponent extends LitElement {
  static get properties() {
    return {
      posts: {
        type: Array,
      },
      selectedPost: {
        type: Object,
        state: true,
      },
    };
  }

  render() {
    return html`<form id="postForm">
      <div class="formRow">
        <label>
          Title:
          <input
            type="text"
            id="title"
            .value="${this.selectedPost?.title || ""}"
          />
        </label>
      </div>
      <div class="formRow">
        <label>
          Content:
          <input
            type="text"
            id="content"
            .value="${this.selectedPost?.content || ""}"
          />
        </label>
      </div>
      <button id="cancelButton" @click="${(e) => this.cancel(e)}">
        Cancel
      </button>
      ${this.selectedPost
        ? html`<button
              id="deleteButton"
              @click="${(e) => this.deletePost(e, this.selectedPost)}"
            >
              Delete
            </button>
            <button
              id="updateButton"
              @click="${(e) => this.updatePost(e, this.selectedPost)}"
            >
              Update
            </button>`
        : html`<button id="addButton" @click="${(e) => this.addPost(e)}">
            Add
          </button>`}
    </form>`;
  }

  cancel(e) {
    e.preventDefault();
    this.selectedPost = null;
  }

  async deletePost(e, selectedPost) {
    e.preventDefault();
    this.posts = await DeletePostUseCase.execute(this.posts, selectedPost.id);
    console.log(
      "🚀 ~ PostActionsComponent ~ deletePost ~ this.posts:",
      this.posts
    );
    this.notifyChangePosts(this.posts);
  }

  async addPost(e) {
    e.preventDefault();
    const title = this.querySelector("#title").value;
    const content = this.querySelector("#content").value;
    this.posts = await AddPostUseCase.execute(this.posts, {
      id: v4(),
      title: title,
      content: content,
    });
    this.notifyChangePosts(this.posts);
  }

  async updatePost(e, selectedPost) {
    e.preventDefault();
    const title = this.querySelector("#title").value;
    const content = this.querySelector("#content").value;
    this.posts = await UpdatePostUseCase.execute(this.posts, {
      id: selectedPost?.id,
      title: title,
      content: content,
    });
    this.notifyChangePosts(this.posts);
  }

  notifyChangePosts(posts) {
    this.dispatchEvent(
      new CustomEvent("change-posts", {
        detail: {
          posts: posts,
        },
      })
    );
    this.clearForm();
  }

  clearForm() {
    this.querySelector("#title").value = "";
    this.querySelector("#content").value = "";
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("genk-posts-actions", PostActionsComponent);
