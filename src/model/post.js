/**
 * @typedef {Object} PostType
 * @property {string} id
 * @property {string} title
 * @property {string} content
 */

export class Post {
  constructor({ id, title, content }) {
    this.id = id;
    this.title = title;
    this.content = content;
  }
}
