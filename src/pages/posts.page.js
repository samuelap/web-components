import "../components/posts-mediator.component";

export class PostsPage extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.innerHTML = `
      <genk-posts-mediator></genk-posts-mediator>    
    `;
  }
}

customElements.define("posts-page", PostsPage);
