import { Post } from "../model/post";
import { PostsRepository } from "../repositories/posts.repository";

export class AddPostUseCase {
  /**
   *
   * @param {import("../model/post").PostType[]} posts
   * @param {import("../model/post").PostType} post
   * @returns Promise<import("../model/post").PostType[]>}
   */
  static async execute(posts, post) {
    const repository = new PostsRepository();
    const newPostApi = await repository.createPost(post.title, post.content);

    /**
     * @type {import("../model/post").PostType}
     */
    const newPostModel = new Post({
      id: post.id,
      title: newPostApi.title,
      content: newPostApi.body,
    });

    return [newPostModel, ...posts];
  }
}
