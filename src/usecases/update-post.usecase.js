import { PostsRepository } from "../repositories/posts.repository";

export class UpdatePostUseCase {
  /**
   *
   * @param {import("../model/post").PostType[]} posts
   * @param {import("../model/post").PostType} postModel
   */
  static async execute(posts = [], postModel) {
    const repository = new PostsRepository();
    await repository.updatePost(postModel);

    return posts.map((post) =>
      post.id === postModel.id ? (post = postModel) : (post = post)
    );
  }
}
