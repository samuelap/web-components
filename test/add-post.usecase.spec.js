import { v4 } from "uuid";

import POSTS from "../fixtures/posts.json";
import { Post } from "../src/model/post";
import { PostsRepository } from "../src/repositories/posts.repository";
import { AddPostUseCase } from "../src/usecases/add-post.usecase";
import { AllPostsUseCase } from "../src/usecases/all-posts.usecase";

jest.mock("../src/repositories/posts.repository");

describe("Add post use case", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should execute properly", async () => {
    /**
     * @type {import("../src/model/post").PostType;}
     */
    const post = new Post({
      id: v4(),
      title: "Titulo test",
      content: "Cuerpo test",
    });

    PostsRepository.mockImplementation(() => {
      return {
        createPost: () => {
          return {
            id: 101,
            title: post.title,
            body: post.content,
          };
        },
        getAllPosts: jest.fn().mockResolvedValue(POSTS),
      };
    });

    const posts = await AllPostsUseCase.execute();

    const postsUpdated = await AddPostUseCase.execute(posts, post);

    expect(postsUpdated.length).toBe(POSTS.length + 1);
    expect(postsUpdated[0].id).toBe(post.id);
    expect(postsUpdated[0].title).toBe(post.title);
    expect(postsUpdated[0].content).toBe(post.content);
  });
});
