import POSTS from "../fixtures/posts.json";
import { PostsRepository } from "../src/repositories/posts.repository";
import { AllPostsUseCase } from "../src/usecases/all-posts.usecase";

jest.mock("../src/repositories/posts.repository");

describe("All posts use case", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should get all posts", async () => {
    PostsRepository.mockImplementation(() => {
      return {
        getAllPosts: () => {
          return POSTS;
        },
      };
    });

    const posts = await AllPostsUseCase.execute();
    console.log("🚀 ~ it ~ posts:", posts);

    expect(posts.length).toBe(100);
  });
});
