import { v4 } from "uuid";

import { PostsRepository } from "../src/repositories/posts.repository";
import { AddPostUseCase } from "../src/usecases/add-post.usecase";
import { AllPostsUseCase } from "../src/usecases/all-posts.usecase";
import { DeletePostUseCase } from "../src/usecases/delete-post.usecase";
import { POSTS } from "./fixtures/posts";

jest.mock("../src/repositories/posts.repository");

describe("Delete use case", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should execute properly", async () => {
    const postId = v4();
    const post = {
      id: postId,
      title: "title test",
      content: "content test",
    };

    PostsRepository.mockImplementation(() => {
      return {
        createPost: jest.fn().mockResolvedValue({
          id: 101,
          title: post.title,
          body: post.content,
        }),
        getAllPosts: jest.fn().mockResolvedValue(POSTS),
        deletePost: () => {},
      };
    });
    const posts = await AllPostsUseCase.execute();
    const postsCreated = await AddPostUseCase.execute(posts, post);
    const postsDeleted = await DeletePostUseCase.execute(postsCreated, postId);
    expect(posts.length).toBe(postsDeleted.length);
  });
});
