import { v4 } from "uuid";

import { PostsRepository } from "../src/repositories/posts.repository";
import { AddPostUseCase } from "../src/usecases/add-post.usecase";
import { AllPostsUseCase } from "../src/usecases/all-posts.usecase";
import { UpdatePostUseCase } from "../src/usecases/update-post.usecase";
import { POSTS } from "./fixtures/posts";

jest.mock("../src/repositories/posts.repository");

describe("Update post use case", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should executing properly", async () => {
    const postId = v4();
    const newPost = {
      id: postId,
      title: "New post title",
      content: "New post content",
    };

    PostsRepository.mockImplementation(() => {
      return {
        createPost: jest.fn().mockResolvedValue({
          id: 101,
          title: newPost.title,
          body: newPost.content,
        }),
        getAllPosts: jest.fn().mockResolvedValue(POSTS),
        updatePost: jest.fn().mockResolvedValue(),
      };
    });
    const postWithChanges = {
      id: postId,
      title: "Post con los cambios",
      content: "Post con contenido cambiado",
    };
    const posts = await AllPostsUseCase.execute();

    const postsWithNewPost = await AddPostUseCase.execute(posts, newPost);

    const postsUpdated = await UpdatePostUseCase.execute(
      postsWithNewPost,
      postWithChanges
    );

    expect(postsUpdated.length).toBe(postsWithNewPost.length);
  });
});
